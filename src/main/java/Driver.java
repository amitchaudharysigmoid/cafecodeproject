import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class Driver extends Configured implements Tool{

    public static void main(String[] args) throws Exception{
        ToolRunner.run(new Driver(), args);
    }

    @Override
    public int run(String[] args) throws Exception{

        List<Scan> scanList = inputList();
        Configuration conf = new Configuration();

        Job job = Job.getInstance(conf, "Cafeteria in emp table");

        FileOutputFormat.setOutputPath(job, new Path("Outputpath"));

        job.setJarByClass(Driver.class);

        TableMapReduceUtil.initTableMapperJob(scanList, CafeMap.class, Text.class,
                ImmutableBytesWritable.class, job);

        TableMapReduceUtil.initTableReducerJob("EmployeesInfo", CafeReduce.class, job);

        return (job.waitForCompletion(true) ? 0 : 1);
    }

    public List<Scan> inputList() throws Exception{

        List<Scan> scanList = new ArrayList<>();
        Scan employeeScan = new Scan();
        employeeScan.setAttribute(Scan.SCAN_ATTRIBUTES_TABLE_NAME, Bytes.toBytes("EmployeesInfo"));
        employeeScan.setCacheBlocks(false);
        scanList.add(employeeScan);

        Scan buildingScan = new Scan();
        buildingScan.setAttribute(Scan.SCAN_ATTRIBUTES_TABLE_NAME, Bytes.toBytes("BuildingsInfo"));
        buildingScan.setCacheBlocks(false);
        scanList.add(buildingScan);
        return scanList;
    }
}

