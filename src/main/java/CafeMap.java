import com.google.protobuf.ByteString;
import com.proto.packageOne.ComponentTagger;
import com.proto.packageOne.Employee1;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.ResultSerialization;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.hbase.mapreduce.TableSplit;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.Text;
import com.proto.packageOne.Building1;
import java.io.IOException;


public class CafeMap extends TableMapper<Text, ImmutableBytesWritable> {

    public void map(ImmutableBytesWritable key, Result result, Context con) throws IOException,InterruptedException{

        TableSplit tableSplit = (TableSplit)con.getInputSplit();
        String tableName = tableSplit.getTable().getNameAsString();
        String employeeTable = "EmployeeInfo";
        String buildingTable = "BuildingInfo";

        if(employeeTable.contains(tableName)){
            Employee.Employee employee= Employee.Employee.parseFrom(result.getValue(
                    Bytes.toBytes("attributes"),Bytes.toBytes("data")));
            ComponentTagger.componenttagger.Builder cb= ComponentTagger.componenttagger.newBuilder();
            cb.setTableName(ComponentTagger.componenttagger.identifier.employeeTable);
            cb.setRowData(ByteString.copyFrom(employee.toByteArray()));
            con.write(new Text(employee.getBuildingCode()),new ImmutableBytesWritable(cb.build().toByteArray()));
        }
        else{
            Building.Building building = Building.Building.parseFrom(result.getValue(
                    Bytes.toBytes("attributes"),Bytes.toBytes("data")));
            ComponentTagger.componenttagger.Builder cb = ComponentTagger.componenttagger.newBuilder();
            cb.setTableName(ComponentTagger.componenttagger.identifier.buildingTable);
            cb.setRowData(ByteString.copyFrom(building.toByteArray()));
            con.write(new Text(building.getBuildingCode()),new ImmutableBytesWritable(cb.build().toByteArray()));
        }
    }
}

