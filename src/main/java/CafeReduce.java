import com.proto.packageOne.Building1;
import com.proto.packageOne.ComponentTagger;
import com.proto.packageOne.Employee1;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableReducer;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.Text;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;



public class CafeReduce extends TableReducer<Text, ImmutableBytesWritable,Text> {
    public void reduce(Text key,Iterable<ImmutableBytesWritable> value, Context con) throws IOException,InterruptedException{
        List<Employee.Employee> employeeList= new ArrayList<>();
        List<Building.Building> buildingList= new ArrayList<>();
        String employeeTable = "EmployeesInfo";
        String buildingTable = "BuildingsInfo";

        for (ImmutableBytesWritable data:value) {
            ComponentTagger.componenttagger ct= ComponentTagger.componenttagger.parseFrom(data.get());
            if(employeeTable.equals(ct.getTableName())){
                employeeList.add(Employee.Employee.parseFrom(ct.getRowData()));
            }
            else{
                buildingList.add(Building.Building.parseFrom(ct.getRowData()));
            }
        }
        if(buildingList.size()==1){
            String ccode=buildingList.get(0).getCafteriaCode();
            for (Employee.Employee emp:employeeList) {
                Employee.Employee.Builder employee = Employee.Employee.newBuilder();
                employee.setName(emp.getName());
                employee.setEmployeeId(emp.getEmployeeId());
                employee.setBuildingCode(emp.getBuildingCode());
                employee.setFloornum(emp.getFloornum());
                employee.setSalary(emp.getSalary());
                employee.setDepartment(emp.getDepartment());
                employee.setCafteriaCode(ccode);
                Configuration config = HBaseConfiguration.create();
                HTable table = new HTable(config, "EmployeesInfo");
                Put p = new Put(Bytes.toBytes(employee.getEmployeeId()));
                p.addColumn(Bytes.toBytes("attributes"), Bytes.toBytes("data"), employee.build().toByteArray());
                table.put(p);
            }
        }
        else
            throw new RuntimeException("Multiple buiding with same code");

    }
}
